import pymysql
import serial
import RPi.GPIO as GPIO
import time

ser=serial.Serial('/dev/ttyACM0',9600)
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)

connection = pymysql.connect(host='db1.ece.iastate.edu',
		user='rroltgen',
		password='p8oeswi3Wroa',
		charset='utf8mb4',
		cursorclass=pymysql.cursors.DictCursor)
		
try:
	with connection.cursor() as cursor:
		doorID = 1
		userID = 1
		sql = "SELECT doorStatusChange FROM rroltgen.doorHistory WHERE doorID=%s AND id=%s"
		cursor.execute(sql, (doorID, userID))
		result = cursor.fetchone()
		new_status = result['doorStatusChange']
		print "History Door Status: " + `new_status`
		sql_select = "SELECT doorStatus FROM rroltgen.doors WHERE doorID=%s"
		cursor.execute(sql_select, (doorID))
		result1 = cursor.fetchone()
		cur_status = result1['doorStatus']
		print "Current Door Status: " + `cur_status`

		if new_status != cur_status:
			sql_update = "UPDATE rroltgen.doors SET doorStatus=%s WHERE doorID=%s"
			cursor.execute(sql_update, (new_status, doorID))
			print "Updated"

			# PySerial Code Here to send new_status result to Arduino
			ser.write(new_status)

		sql_select1 = "SELECT doorStatus FROM rroltgen.doors WHERE doorID=%s"
		cursor.execute(sql_select1, (doorID))
		result2 = cursor.fetchone()
		up_status = result2['doorStatus']
		print "Updated Door Status: " + `up_status`

	connection.commit()
finally:
	connection.close()