import serial
import RPi.GPIO as GPIO
import time
ser=serial.Serial('/dev/ttyACM0',9600)
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(11,GPIO.IN)
GPIO.setup(26,GPIO.IN)
 
while True:
    if (GPIO.input(11)==1):
        ser.write('1')
    else:
        time.sleep(.1)
    if (GPIO.input(26)==1):
        ser.write('2')
    else:
        time.sleep(.1)
    if (GPIO.input(11)==0 and GPIO.input(26)==0):
        ser.write('0')
    else:
        time.sleep(.1)
    time.sleep(.25)