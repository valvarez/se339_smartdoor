#include <Servo.h>  // servo library


Servo servo;  // servo control object

boolean locked = true; //State of lock - default is locked

char serInput = '0'; //char to hold serial input character - default is 0

void setup()
{
  Serial.begin(9600); //Starting Serial communication

  servo.attach(9); //Attach servo control to pin 9

  servo.write(180); //Default servo to lock position
}


void loop()
{  
  if(serInput == '1' && locked == false) //Signal says lock
  {
    servo.write(180);
    delay(500);
    locked = true;
    serInput = '0';
    Serial.write('1');
  }
  else if(serInput == '2' && locked == true) //Signal says unlock
  {
    servo.write(0);
    delay(500);
    locked = false;
    serInput = '0';
    Serial.write('2');
  }
}

void serialRead() //Reads serial input between loops
{
  while (Serial.available())
  {
    serInput = (char)Serial.read();
  }
}

