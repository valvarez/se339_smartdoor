var currentStatus = "CLOSED"; //TODO: replace with server call
var initialize = 0;
function updateStatus() {
    switch(currentStatus){
        case "OPEN":
            unlockDoor();
            break;
        case "CLOSED":
            lockDoor();
            break;
        default:
            printError();
    }
}
function unlockDoor(){
    currentStatus = "OPEN";
    document.getElementById("doorStatus").innerHTML = currentStatus;
    $("#statusImage").attr("src", "http://l.thumbs.canstockphoto.com/canstock2553396.jpg");
    $("#unlockButton").hide();
    $("#lockButton").show();
    if(initialize == 0){
        initialize = 1;
    }else{
        sendRequest(1);
    }
}
function lockDoor(){
    currentStatus = "CLOSED";
    document.getElementById("doorStatus").innerHTML = currentStatus;
    $("#statusImage").attr("src", "http://www.corvallisadvocate.com/wp-content/uploads/2015/08/closed-door.jpg");
    $("#statusImage").attr("height", 150);
    $("#statusImage").attr("width", 135);
    $("#lockButton").hide();
    $("#unlockButton").show();
    if(initialize == 0){
        initialize = 1;
    }else{
        sendRequest(0);
    }
}
function printError(){
    //TODO: print error
}
function sendRequest(doorStatus){
    var url = "http://localhost/se339_smartdoor/api/update_door_history.php?doorID=1&doorStatusChange=" + doorStatus;
    console.log(url);
    $.get(url, function(data, status){
        console.log('!');
    });
}
